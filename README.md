![Theme Screenshot](screenshot.png)
[Wordpress Basic Theme](http://https://github.com/StuLast/wordpress-theme-basic)


# Basic Wordpress Theme

A basic Wordpress theme from which to build other themes.  Simple, clean and quick.

## Installation

### Option 1 - Cloning
Using gitbash or Git UI navigate to your /wp-content/themes folder and execute the following command:
```
git clone git@github.com:StuLast/wordpress-theme-basic.git basic-theme

```

The target `basic-theme` at the end can be anything you like, but should reflect the theme name in some way.

### Option 2 - Downloading

Visit [https://github.com/StuLast/wordpress-theme-basic](https://github.com/StuLast/wordpress-theme-basic) and select 'download zip' from the green button.  You can include this in your '/wp-content/themes' folder.  You can change the name of the theme folder itself to anything you like, but it should reflect the name of the theme in some way.

## Licenses

See LICENSE file in the theme directory.

##  Author

Visit [http://stulast.co.uk](http://stulast.co.uk) for more information about the author