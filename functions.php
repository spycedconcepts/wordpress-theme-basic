<?php

function basic_theme_setup(){
	// Featured Image Support
	add_theme_support('post-thumbnails');

	// Menus
	register_nav_menus(array(
		'primary'	=> __('Primary Menu')
	));
}

add_action('after_setup_theme', 'basic_theme_setup');

// Excerpt Length
function set_excerpt_length(){
	return 60;
}

add_filter('excerpt_length', 'set_excerpt_length');

// Widget Locations
function init_widgets($id){
	register_sidebar(array(
		'name'	=> 'Sidebar',
		'id'	=> 'sidebar',
		'before_widget'	=> '<div class="side-widget">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<div class="side-widget-title"> <h3>',
		'after_title'	=>	'</h3> </div>'
	));
}

add_action('widgets_init', 'init_widgets');

//Bespoke functions

function basic_theme_tint_first_word($text, $squeeze = False) {
	$textWords = explode(' ', $text);
	$textFirstWord = $textWords[0] . ($squeeze? '': ' ');
	$remaingWordCount = count($textWords)-1;
	$textOtherWords = implode(' ', array_slice($textWords, 1, $remaingWordCount));
	return '<span class="first-word">'.$textFirstWord.'</span>' . ($textOtherWords ? $textOtherWords : '');
}
