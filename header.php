<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php bloginfo('name'); ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="container">
			<?php 
				$titleWords = explode(' ', get_bloginfo('name'));
				$titleFirstWord = $titleWords[0];
				$remaingWordCount = count($titleWords)-1;
				$titleOtherWords = implode(' ', array_slice($titleWords, 1, $remaingWordCount));
			?>
			<h1><?php echo basic_theme_tint_first_word(get_bloginfo('name'), True); ?></h1>
			<span><?php bloginfo('description'); ?></span>
		</div>
	</header>
	<nav class="main-nav">
		<div class="container">
		<?php
			$args = array(
				'theme_location'	=> 'primary'
			);
		?>
		<?php wp_nav_menu($args); ?>
		</div>
	</nav>