<?php get_header(); ?>
<div class="container">
	<div class="main">
			<?php if(have_posts()) : ?>
				<?php while(have_posts()): the_post(); ?>
					<?php get_template_part('content'); ?>
				<?php endwhile; ?>
			<?php else : ?>
				<?php echo wpautop('Sorry, No posts were found'); ?>
			<?php endif; ?>
	</div>

<?php get_footer(); ?>