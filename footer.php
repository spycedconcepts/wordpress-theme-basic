<div class="sidebar">
			<?php if(is_active_sidebar('sidebar')) : ?>
				<?php dynamic_sidebar('sidebar'); ?>
			<?php endif; ?>
		</div>

		<div class="clr"></div>

	</div>

<footer>
	<div class="container">
		<p><?php echo basic_theme_tint_first_word(get_bloginfo('name'), True); ?> &copy; <?php echo date('Y'); ?> </p>
		A Wordpress Theme by <a href="http://stulast.co.uk" target="_blank">Stu Last</a>
	</div>
</footer>

	<?php wp_footer(); ?>
</body>
</html>